package net.lil.catboys.api

import com.squareup.moshi.Json

data class CatboyPhoto(
    val url: String,
    val artist: String,
    @Json(name = "artist_url") val artistUrl: String,
    @Json(name = "source_url") val sourceUrl: String,
    val error: String
)
