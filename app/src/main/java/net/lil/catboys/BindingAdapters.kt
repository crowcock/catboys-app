package net.lil.catboys

import android.view.View
import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import coil.load
import net.lil.catboys.ui.CbApiStatus

@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
        imgView.load(imgUri) {
            placeholder(R.drawable.loading_animation)
            error(R.drawable.ic_broken_image)
        }
    }
}

@BindingAdapter("cbApiStatus")
fun bindStatus(statusImageView: ImageView, status: CbApiStatus?) {
    when (status) {
        CbApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        CbApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        CbApiStatus.API_ERROR -> {
            // TODO: api error
        }
        CbApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
        null -> {}
    }
}
