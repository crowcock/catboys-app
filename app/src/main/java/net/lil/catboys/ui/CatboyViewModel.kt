package net.lil.catboys.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import net.lil.catboys.api.CatboyApi
import net.lil.catboys.api.CatboyPhoto

enum class CbApiStatus { LOADING, ERROR, API_ERROR, DONE }

/**
 * The [ViewModel] that is attached to the [CatboyFragment].
 */
class CatboyViewModel : ViewModel() {

    private val _photo = MutableLiveData<CatboyPhoto>()
    val photo: LiveData<CatboyPhoto> = _photo

    private val _status = MutableLiveData<CbApiStatus>()
    val status: LiveData<CbApiStatus> = _status


    /**
     * Call getCbPhotos() on init so we can display status immediately.
     */
    init {
        getCbPhotos()
    }

    /**
     * Gets Mars photos information from the Catboys API Retrofit service and updates the
     * [CatboyPhoto] [List] [LiveData].
     */
    fun getCbPhotos() {
        viewModelScope.launch {
            _status.value = CbApiStatus.LOADING
            try {
                _photo.value = CatboyApi.retrofitService.getPhoto()

                if (_photo.value?.error != "none") {
                    _status.value = CbApiStatus.API_ERROR
                } else {
                    _status.value = CbApiStatus.DONE
                }
            } catch (e: Exception) {
                _status.value = CbApiStatus.ERROR
            }
        }
    }
}
