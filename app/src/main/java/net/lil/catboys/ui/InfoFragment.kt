package net.lil.catboys.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import net.lil.catboys.databinding.FragmentInfoBinding


class InfoFragment : Fragment() {

    private lateinit var binding: FragmentInfoBinding
    private val viewModel: CatboyViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentInfoBinding.inflate(inflater)

        // Allows Data Binding to observe LiveData with the lifecycle of this Fragment
        binding.lifecycleOwner = this

        // Giving the binding access to the CatboyViewModel
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.saveButton.setOnClickListener {
            val link = viewModel.photo.value?.url

            if (link == null) {
                Toast.makeText(
                    requireContext(), "Image is unavailable", Toast.LENGTH_SHORT
                ).show()
            } else {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
                startActivity(browserIntent)
            }
        }

        binding.linkButton.setOnClickListener {
            val link = viewModel.photo.value?.sourceUrl ?: viewModel.photo.value?.artistUrl

            if (link == null || link == "none") {
                Toast.makeText(
                    requireContext(), "Source link is unavailable", Toast.LENGTH_SHORT
                ).show()
            } else {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
                startActivity(browserIntent)
            }
        }

        binding.backButton.setOnClickListener {
            val action = InfoFragmentDirections.actionInfoFragmentToCatboyFragment()
            findNavController().navigate(action)
        }
    }
}
