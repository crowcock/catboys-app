package net.lil.catboys.ui

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import net.lil.catboys.R
import net.lil.catboys.databinding.FragmentCatboyBinding

/**
 * This fragment shows the the status of the Catboy photos web services transaction.
 */
class CatboyFragment : Fragment() {

    private lateinit var binding: FragmentCatboyBinding
    private val viewModel: CatboyViewModel by activityViewModels()

    /**
     * Inflates the layout with Data Binding, sets its lifecycle owner to the CatboyFragment
     * to enable Data Binding to observe LiveData.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCatboyBinding.inflate(inflater)

        // Allows Data Binding to observe LiveData with the lifecycle of this Fragment
        binding.lifecycleOwner = this

        // Giving the binding access to the CatboyViewModel
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.cbImage.setOnClickListener {
            val action = CatboyFragmentDirections.actionCatboyFragmentToInfoFragment()
            findNavController().navigate(action)
        }

        binding.swipeRefresh.setOnRefreshListener {
            viewModel.getCbPhotos()

            // rids screen of prolonged refresh spinner
            binding.swipeRefresh.isRefreshing = false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.f_catboy, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_refresh -> {
                binding.swipeRefresh.isRefreshing = true
                viewModel.getCbPhotos()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
